﻿
namespace WinformGeneralDeveloperFrame
{
    partial class frmSelectDataBase
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.completionWizardPage1 = new DevExpress.XtraWizard.CompletionWizardPage();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.isEntity = new DevExpress.XtraEditors.CheckEdit();
            this.isDB = new DevExpress.XtraEditors.CheckEdit();
            this.isWinform = new DevExpress.XtraEditors.CheckEdit();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.tableName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.dataBaseFieldName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.CSharpFieldName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.controlType = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox1 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.gridColumn4 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemComboBox2 = new DevExpress.XtraEditors.Repository.RepositoryItemComboBox();
            this.controlName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.isVisible = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit1 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.isReadonly = new DevExpress.XtraGrid.Columns.GridColumn();
            this.repositoryItemCheckEdit2 = new DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit();
            this.isKey = new DevExpress.XtraGrid.Columns.GridColumn();
            this.isIdentity = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn3 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.textEdit2 = new DevExpress.XtraEditors.TextEdit();
            this.textEdit1 = new DevExpress.XtraEditors.TextEdit();
            this.Root = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutControlItem1 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem2 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem4 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem5 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem7 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem3 = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutControlItem6 = new DevExpress.XtraLayout.LayoutControlItem();
            this.wizardPage1 = new DevExpress.XtraWizard.WizardPage();
            this.cmbtablelist = new DevExpress.XtraEditors.CheckedComboBoxEdit();
            this.welcomeWizardPage1 = new DevExpress.XtraWizard.WelcomeWizardPage();
            this.txtConnurl = new DevExpress.XtraEditors.MemoEdit();
            this.btnAdd = new DevExpress.XtraEditors.SimpleButton();
            this.cmbDataBaseList = new DevExpress.XtraEditors.ComboBoxEdit();
            this.label1 = new System.Windows.Forms.Label();
            this.wizardControl1 = new DevExpress.XtraWizard.WizardControl();
            this.xtraFolderBrowserDialog1 = new DevExpress.XtraEditors.XtraFolderBrowserDialog(this.components);
            this.completionWizardPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.isEntity.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isDB.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.isWinform.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).BeginInit();
            this.wizardPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbtablelist.Properties)).BeginInit();
            this.welcomeWizardPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtConnurl.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDataBaseList.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.wizardControl1)).BeginInit();
            this.wizardControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // completionWizardPage1
            // 
            this.completionWizardPage1.Controls.Add(this.layoutControl1);
            this.completionWizardPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.completionWizardPage1.Name = "completionWizardPage1";
            this.completionWizardPage1.Size = new System.Drawing.Size(814, 461);
            this.completionWizardPage1.Text = "明细设置";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.isEntity);
            this.layoutControl1.Controls.Add(this.isDB);
            this.layoutControl1.Controls.Add(this.isWinform);
            this.layoutControl1.Controls.Add(this.gridControl1);
            this.layoutControl1.Controls.Add(this.simpleButton1);
            this.layoutControl1.Controls.Add(this.textEdit2);
            this.layoutControl1.Controls.Add(this.textEdit1);
            this.layoutControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.layoutControl1.Location = new System.Drawing.Point(0, 0);
            this.layoutControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.Root;
            this.layoutControl1.Size = new System.Drawing.Size(814, 461);
            this.layoutControl1.TabIndex = 0;
            this.layoutControl1.Text = "layoutControl1";
            // 
            // isEntity
            // 
            this.isEntity.Location = new System.Drawing.Point(527, 16);
            this.isEntity.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.isEntity.Name = "isEntity";
            this.isEntity.Properties.Caption = "生成实体类";
            this.isEntity.Size = new System.Drawing.Size(97, 22);
            this.isEntity.StyleController = this.layoutControl1;
            this.isEntity.TabIndex = 10;
            // 
            // isDB
            // 
            this.isDB.Location = new System.Drawing.Point(714, 16);
            this.isDB.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.isDB.Name = "isDB";
            this.isDB.Properties.Caption = "生成EFDB";
            this.isDB.Size = new System.Drawing.Size(87, 22);
            this.isDB.StyleController = this.layoutControl1;
            this.isDB.TabIndex = 9;
            // 
            // isWinform
            // 
            this.isWinform.Location = new System.Drawing.Point(628, 16);
            this.isWinform.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.isWinform.Name = "isWinform";
            this.isWinform.Properties.Caption = "生成界面";
            this.isWinform.Size = new System.Drawing.Size(82, 22);
            this.isWinform.StyleController = this.layoutControl1;
            this.isWinform.TabIndex = 9;
            // 
            // gridControl1
            // 
            this.gridControl1.EmbeddedNavigator.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.gridControl1.Location = new System.Drawing.Point(13, 49);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.RepositoryItems.AddRange(new DevExpress.XtraEditors.Repository.RepositoryItem[] {
            this.repositoryItemCheckEdit1,
            this.repositoryItemCheckEdit2,
            this.repositoryItemComboBox1,
            this.repositoryItemComboBox2});
            this.gridControl1.Size = new System.Drawing.Size(788, 396);
            this.gridControl1.TabIndex = 7;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.tableName,
            this.dataBaseFieldName,
            this.CSharpFieldName,
            this.controlType,
            this.gridColumn4,
            this.controlName,
            this.gridColumn1,
            this.isVisible,
            this.isReadonly,
            this.isKey,
            this.isIdentity,
            this.gridColumn2,
            this.gridColumn3});
            this.gridView1.DetailHeight = 450;
            this.gridView1.FixedLineWidth = 3;
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.GroupCount = 1;
            this.gridView1.Name = "gridView1";
            this.gridView1.SortInfo.AddRange(new DevExpress.XtraGrid.Columns.GridColumnSortInfo[] {
            new DevExpress.XtraGrid.Columns.GridColumnSortInfo(this.tableName, DevExpress.Data.ColumnSortOrder.Ascending)});
            // 
            // tableName
            // 
            this.tableName.Caption = "表名";
            this.tableName.FieldName = "tableName";
            this.tableName.MinWidth = 23;
            this.tableName.Name = "tableName";
            this.tableName.Visible = true;
            this.tableName.VisibleIndex = 0;
            this.tableName.Width = 86;
            // 
            // dataBaseFieldName
            // 
            this.dataBaseFieldName.Caption = "数据库字段名";
            this.dataBaseFieldName.FieldName = "dataBaseFieldName";
            this.dataBaseFieldName.MinWidth = 23;
            this.dataBaseFieldName.Name = "dataBaseFieldName";
            this.dataBaseFieldName.Visible = true;
            this.dataBaseFieldName.VisibleIndex = 0;
            this.dataBaseFieldName.Width = 86;
            // 
            // CSharpFieldName
            // 
            this.CSharpFieldName.Caption = "C#字段名";
            this.CSharpFieldName.FieldName = "CSharpFieldName";
            this.CSharpFieldName.MinWidth = 23;
            this.CSharpFieldName.Name = "CSharpFieldName";
            this.CSharpFieldName.Visible = true;
            this.CSharpFieldName.VisibleIndex = 1;
            this.CSharpFieldName.Width = 86;
            // 
            // controlType
            // 
            this.controlType.Caption = "控件类型";
            this.controlType.ColumnEdit = this.repositoryItemComboBox1;
            this.controlType.FieldName = "controlType";
            this.controlType.MinWidth = 23;
            this.controlType.Name = "controlType";
            this.controlType.Visible = true;
            this.controlType.VisibleIndex = 2;
            this.controlType.Width = 86;
            // 
            // repositoryItemComboBox1
            // 
            this.repositoryItemComboBox1.AutoHeight = false;
            this.repositoryItemComboBox1.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox1.Items.AddRange(new object[] {
            "TextEdit",
            "DateEdit",
            "CheckEdit",
            "MemoEdit",
            "LookUpEdit",
            "TreeListLookUpEdit",
            "ComboBoxEdit",
            "CheckedComboBoxEdit"});
            this.repositoryItemComboBox1.Name = "repositoryItemComboBox1";
            // 
            // gridColumn4
            // 
            this.gridColumn4.Caption = "数据源";
            this.gridColumn4.ColumnEdit = this.repositoryItemComboBox2;
            this.gridColumn4.FieldName = "DataTableName";
            this.gridColumn4.MinWidth = 23;
            this.gridColumn4.Name = "gridColumn4";
            this.gridColumn4.Visible = true;
            this.gridColumn4.VisibleIndex = 3;
            this.gridColumn4.Width = 86;
            // 
            // repositoryItemComboBox2
            // 
            this.repositoryItemComboBox2.AutoHeight = false;
            this.repositoryItemComboBox2.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.repositoryItemComboBox2.Name = "repositoryItemComboBox2";
            // 
            // controlName
            // 
            this.controlName.Caption = "控件name";
            this.controlName.FieldName = "controlName";
            this.controlName.MinWidth = 23;
            this.controlName.Name = "controlName";
            this.controlName.Visible = true;
            this.controlName.VisibleIndex = 4;
            this.controlName.Width = 86;
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "label名称";
            this.gridColumn1.FieldName = "controlLabelName";
            this.gridColumn1.MinWidth = 23;
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 10;
            this.gridColumn1.Width = 86;
            // 
            // isVisible
            // 
            this.isVisible.Caption = "是否可见";
            this.isVisible.ColumnEdit = this.repositoryItemCheckEdit1;
            this.isVisible.FieldName = "isVisible";
            this.isVisible.MinWidth = 23;
            this.isVisible.Name = "isVisible";
            this.isVisible.Visible = true;
            this.isVisible.VisibleIndex = 5;
            this.isVisible.Width = 86;
            // 
            // repositoryItemCheckEdit1
            // 
            this.repositoryItemCheckEdit1.AutoHeight = false;
            this.repositoryItemCheckEdit1.Name = "repositoryItemCheckEdit1";
            // 
            // isReadonly
            // 
            this.isReadonly.Caption = "是否可编辑";
            this.isReadonly.ColumnEdit = this.repositoryItemCheckEdit2;
            this.isReadonly.FieldName = "isEdit";
            this.isReadonly.MinWidth = 23;
            this.isReadonly.Name = "isReadonly";
            this.isReadonly.Visible = true;
            this.isReadonly.VisibleIndex = 6;
            this.isReadonly.Width = 86;
            // 
            // repositoryItemCheckEdit2
            // 
            this.repositoryItemCheckEdit2.AutoHeight = false;
            this.repositoryItemCheckEdit2.Name = "repositoryItemCheckEdit2";
            // 
            // isKey
            // 
            this.isKey.Caption = "是否主键";
            this.isKey.FieldName = "isKey";
            this.isKey.MinWidth = 23;
            this.isKey.Name = "isKey";
            this.isKey.OptionsColumn.ReadOnly = true;
            this.isKey.Visible = true;
            this.isKey.VisibleIndex = 7;
            this.isKey.Width = 86;
            // 
            // isIdentity
            // 
            this.isIdentity.Caption = "是否自增";
            this.isIdentity.FieldName = "isIdentity";
            this.isIdentity.MinWidth = 23;
            this.isIdentity.Name = "isIdentity";
            this.isIdentity.OptionsColumn.ReadOnly = true;
            this.isIdentity.Visible = true;
            this.isIdentity.VisibleIndex = 9;
            this.isIdentity.Width = 86;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "是否搜索列";
            this.gridColumn2.FieldName = "isSearch";
            this.gridColumn2.MinWidth = 23;
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 11;
            this.gridColumn2.Width = 86;
            // 
            // gridColumn3
            // 
            this.gridColumn3.Caption = "是否为空检验";
            this.gridColumn3.FieldName = "isCheck";
            this.gridColumn3.MinWidth = 23;
            this.gridColumn3.Name = "gridColumn3";
            this.gridColumn3.Visible = true;
            this.gridColumn3.VisibleIndex = 8;
            this.gridColumn3.Width = 86;
            // 
            // simpleButton1
            // 
            this.simpleButton1.Location = new System.Drawing.Point(443, 16);
            this.simpleButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(80, 27);
            this.simpleButton1.StyleController = this.layoutControl1;
            this.simpleButton1.TabIndex = 6;
            this.simpleButton1.Text = "浏览";
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // textEdit2
            // 
            this.textEdit2.Location = new System.Drawing.Point(230, 16);
            this.textEdit2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit2.Name = "textEdit2";
            this.textEdit2.Properties.ReadOnly = true;
            this.textEdit2.Size = new System.Drawing.Size(209, 24);
            this.textEdit2.StyleController = this.layoutControl1;
            this.textEdit2.TabIndex = 5;
            // 
            // textEdit1
            // 
            this.textEdit1.Location = new System.Drawing.Point(76, 16);
            this.textEdit1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textEdit1.Name = "textEdit1";
            this.textEdit1.Size = new System.Drawing.Size(87, 24);
            this.textEdit1.StyleController = this.layoutControl1;
            this.textEdit1.TabIndex = 4;
            // 
            // Root
            // 
            this.Root.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.Root.GroupBordersVisible = false;
            this.Root.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutControlItem1,
            this.layoutControlItem2,
            this.layoutControlItem4,
            this.layoutControlItem5,
            this.layoutControlItem7,
            this.layoutControlItem3,
            this.layoutControlItem6});
            this.Root.Name = "Root";
            this.Root.Size = new System.Drawing.Size(814, 461);
            this.Root.TextVisible = false;
            // 
            // layoutControlItem1
            // 
            this.layoutControlItem1.Control = this.textEdit1;
            this.layoutControlItem1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlItem1.Name = "layoutControlItem1";
            this.layoutControlItem1.Size = new System.Drawing.Size(154, 33);
            this.layoutControlItem1.Text = "命名空间";
            this.layoutControlItem1.TextSize = new System.Drawing.Size(60, 18);
            // 
            // layoutControlItem2
            // 
            this.layoutControlItem2.Control = this.textEdit2;
            this.layoutControlItem2.Location = new System.Drawing.Point(154, 0);
            this.layoutControlItem2.Name = "layoutControlItem2";
            this.layoutControlItem2.Size = new System.Drawing.Size(276, 33);
            this.layoutControlItem2.Text = "输出目录";
            this.layoutControlItem2.TextSize = new System.Drawing.Size(60, 18);
            // 
            // layoutControlItem4
            // 
            this.layoutControlItem4.Control = this.gridControl1;
            this.layoutControlItem4.Location = new System.Drawing.Point(0, 33);
            this.layoutControlItem4.Name = "layoutControlItem4";
            this.layoutControlItem4.Size = new System.Drawing.Size(792, 402);
            this.layoutControlItem4.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem4.TextVisible = false;
            // 
            // layoutControlItem5
            // 
            this.layoutControlItem5.Control = this.isWinform;
            this.layoutControlItem5.Location = new System.Drawing.Point(615, 0);
            this.layoutControlItem5.Name = "layoutControlItem5";
            this.layoutControlItem5.Size = new System.Drawing.Size(86, 33);
            this.layoutControlItem5.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem5.TextVisible = false;
            // 
            // layoutControlItem7
            // 
            this.layoutControlItem7.Control = this.isEntity;
            this.layoutControlItem7.Location = new System.Drawing.Point(514, 0);
            this.layoutControlItem7.Name = "layoutControlItem7";
            this.layoutControlItem7.Size = new System.Drawing.Size(101, 33);
            this.layoutControlItem7.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem7.TextVisible = false;
            // 
            // layoutControlItem3
            // 
            this.layoutControlItem3.Control = this.simpleButton1;
            this.layoutControlItem3.Location = new System.Drawing.Point(430, 0);
            this.layoutControlItem3.Name = "layoutControlItem3";
            this.layoutControlItem3.Size = new System.Drawing.Size(84, 33);
            this.layoutControlItem3.Text = "浏览";
            this.layoutControlItem3.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem3.TextVisible = false;
            // 
            // layoutControlItem6
            // 
            this.layoutControlItem6.Control = this.isDB;
            this.layoutControlItem6.Location = new System.Drawing.Point(701, 0);
            this.layoutControlItem6.Name = "layoutControlItem6";
            this.layoutControlItem6.Size = new System.Drawing.Size(91, 33);
            this.layoutControlItem6.TextSize = new System.Drawing.Size(0, 0);
            this.layoutControlItem6.TextVisible = false;
            // 
            // wizardPage1
            // 
            this.wizardPage1.Controls.Add(this.cmbtablelist);
            this.wizardPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.wizardPage1.Name = "wizardPage1";
            this.wizardPage1.Size = new System.Drawing.Size(814, 461);
            this.wizardPage1.Text = "选择要生成的数据库表";
            // 
            // cmbtablelist
            // 
            this.cmbtablelist.EditValue = "";
            this.cmbtablelist.Location = new System.Drawing.Point(14, 24);
            this.cmbtablelist.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbtablelist.Name = "cmbtablelist";
            this.cmbtablelist.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbtablelist.Size = new System.Drawing.Size(609, 24);
            this.cmbtablelist.TabIndex = 0;
            // 
            // welcomeWizardPage1
            // 
            this.welcomeWizardPage1.Controls.Add(this.txtConnurl);
            this.welcomeWizardPage1.Controls.Add(this.btnAdd);
            this.welcomeWizardPage1.Controls.Add(this.cmbDataBaseList);
            this.welcomeWizardPage1.Controls.Add(this.label1);
            this.welcomeWizardPage1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.welcomeWizardPage1.Name = "welcomeWizardPage1";
            this.welcomeWizardPage1.Size = new System.Drawing.Size(814, 461);
            this.welcomeWizardPage1.Text = "选择您的数据连接";
            // 
            // txtConnurl
            // 
            this.txtConnurl.Location = new System.Drawing.Point(30, 139);
            this.txtConnurl.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.txtConnurl.Name = "txtConnurl";
            this.txtConnurl.Properties.ReadOnly = true;
            this.txtConnurl.Size = new System.Drawing.Size(607, 123);
            this.txtConnurl.TabIndex = 11;
            // 
            // btnAdd
            // 
            this.btnAdd.Location = new System.Drawing.Point(496, 21);
            this.btnAdd.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(141, 30);
            this.btnAdd.TabIndex = 10;
            this.btnAdd.Text = "新建数据连接";
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cmbDataBaseList
            // 
            this.cmbDataBaseList.Location = new System.Drawing.Point(30, 22);
            this.cmbDataBaseList.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.cmbDataBaseList.Name = "cmbDataBaseList";
            this.cmbDataBaseList.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.cmbDataBaseList.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.cmbDataBaseList.Size = new System.Drawing.Size(427, 24);
            this.cmbDataBaseList.TabIndex = 9;
            this.cmbDataBaseList.SelectedIndexChanged += new System.EventHandler(this.comboBoxEdit1_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 103);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "连接字符串:";
            // 
            // wizardControl1
            // 
            this.wizardControl1.CancelText = "取消";
            this.wizardControl1.Controls.Add(this.welcomeWizardPage1);
            this.wizardControl1.Controls.Add(this.wizardPage1);
            this.wizardControl1.Controls.Add(this.completionWizardPage1);
            this.wizardControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.wizardControl1.FinishText = "完成";
            this.wizardControl1.HelpText = "帮助";
            this.wizardControl1.ImageWidth = 247;
            this.wizardControl1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.wizardControl1.MinimumSize = new System.Drawing.Size(134, 139);
            this.wizardControl1.Name = "wizardControl1";
            this.wizardControl1.NextText = "下一步";
            this.wizardControl1.Pages.AddRange(new DevExpress.XtraWizard.BaseWizardPage[] {
            this.welcomeWizardPage1,
            this.wizardPage1,
            this.completionWizardPage1});
            this.wizardControl1.PreviousText = "后退";
            this.wizardControl1.Size = new System.Drawing.Size(888, 672);
            this.wizardControl1.Text = "后退";
            this.wizardControl1.WizardStyle = DevExpress.XtraWizard.WizardStyle.WizardAero;
            this.wizardControl1.CancelClick += new System.ComponentModel.CancelEventHandler(this.wizardControl1_CancelClick);
            this.wizardControl1.FinishClick += new System.ComponentModel.CancelEventHandler(this.wizardControl1_FinishClick);
            this.wizardControl1.NextClick += new DevExpress.XtraWizard.WizardCommandButtonClickEventHandler(this.wizardControl1_NextClick);
            // 
            // xtraFolderBrowserDialog1
            // 
            this.xtraFolderBrowserDialog1.SelectedPath = "xtraFolderBrowserDialog1";
            // 
            // frmSelectDataBase
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(888, 716);
            this.Controls.Add(this.wizardControl1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Margin = new System.Windows.Forms.Padding(3, 5, 3, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSelectDataBase";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "代码生成";
            this.Load += new System.EventHandler(this.frmSelectDataBase_Load);
            this.Controls.SetChildIndex(this.wizardControl1, 0);
            this.completionWizardPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.isEntity.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isDB.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.isWinform.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemComboBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.repositoryItemCheckEdit2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit2.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.textEdit1.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Root)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlItem6)).EndInit();
            this.wizardPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.cmbtablelist.Properties)).EndInit();
            this.welcomeWizardPage1.ResumeLayout(false);
            this.welcomeWizardPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtConnurl.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDataBaseList.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.wizardControl1)).EndInit();
            this.wizardControl1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevExpress.XtraWizard.CompletionWizardPage completionWizardPage1;
        private DevExpress.XtraWizard.WizardPage wizardPage1;
        private DevExpress.XtraEditors.CheckedComboBoxEdit cmbtablelist;
        private DevExpress.XtraWizard.WelcomeWizardPage welcomeWizardPage1;
        private DevExpress.XtraEditors.MemoEdit txtConnurl;
        private DevExpress.XtraEditors.SimpleButton btnAdd;
        private DevExpress.XtraEditors.ComboBoxEdit cmbDataBaseList;
        private System.Windows.Forms.Label label1;
        private DevExpress.XtraWizard.WizardControl wizardControl1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private DevExpress.XtraLayout.LayoutControlGroup Root;
        private DevExpress.XtraEditors.TextEdit textEdit1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem1;
        private DevExpress.XtraEditors.TextEdit textEdit2;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem2;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem4;
        private DevExpress.XtraEditors.XtraFolderBrowserDialog xtraFolderBrowserDialog1;
        private DevExpress.XtraGrid.Columns.GridColumn tableName;
        private DevExpress.XtraGrid.Columns.GridColumn dataBaseFieldName;
        private DevExpress.XtraGrid.Columns.GridColumn CSharpFieldName;
        private DevExpress.XtraGrid.Columns.GridColumn controlType;
        private DevExpress.XtraGrid.Columns.GridColumn controlName;
        private DevExpress.XtraGrid.Columns.GridColumn isVisible;
        private DevExpress.XtraGrid.Columns.GridColumn isReadonly;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit1;
        private DevExpress.XtraEditors.Repository.RepositoryItemCheckEdit repositoryItemCheckEdit2;
        private DevExpress.XtraGrid.Columns.GridColumn isKey;
        private DevExpress.XtraGrid.Columns.GridColumn isIdentity;
        private DevExpress.XtraEditors.CheckEdit isWinform;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem5;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.CheckEdit isEntity;
        private DevExpress.XtraEditors.CheckEdit isDB;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem7;
        private DevExpress.XtraLayout.LayoutControlItem layoutControlItem6;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn4;
        private DevExpress.XtraEditors.Repository.RepositoryItemComboBox repositoryItemComboBox2;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn3;
    }
}